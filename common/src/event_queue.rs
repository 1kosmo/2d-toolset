////////// Std lib //////////
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::hash::Hash;
use std::sync::mpsc::{channel, Receiver, Sender};
////////// My libs //////////
// pub mod channel_group;

pub struct EventQueue<T: Hash + Eq + Copy>
{
    main_receiver: Receiver<T>,
    main_sender_copy: Sender<T>,

    filtered_receivers: HashMap<T, Vec<Sender<T>>>
}

impl<T: Hash + Eq + Copy> EventQueue<T>
{
    pub fn new() -> EventQueue<T>
    {
        let (sender, receiver) = channel();
        EventQueue
        {
            main_receiver: receiver,
            main_sender_copy: sender,
            filtered_receivers: HashMap::new(),
        }
    }

    pub fn get_sender(&self) -> Sender<T>
    {
        self.main_sender_copy.clone()
    }

    pub fn register_receiver(&mut self, listen_for: Vec<T>) -> Receiver<T>
    {
        let (sender, receiver) = channel();

        for msg in listen_for
        {            
            let mut senders = match self.filtered_receivers.entry(msg)
            {
                Entry::Occupied(o) => o.into_mut(),
                Entry::Vacant(v) => v.insert(Vec::new()),
            };

            senders.push(sender.clone());
        }

        receiver
    }

    pub fn process_events(self)
    {
        // The loop stops once every receiver has been closed and recv()
        // throws an error
        loop
        {
            match self.main_receiver.recv()
            {
                Ok(msg_type) =>
                {
                    match self.filtered_receivers.get(&msg_type)
                    {
                        Some(senders) =>
                        {
                            for sender in senders
                            {
                                sender.send(msg_type).unwrap();
                            }                            
                        },
                        None => {}
                    }

                },
                Err(_) => { break }
            }
        }
    }
}

// pub struct SelectGroup<T>
// {
//     receivers: Vec<Receiver<T>>,
//     senders: Vec<Sender<T>>,
//     selection_receiver: Receiver<usize>,
//     selection_sender: Sender<usize>,
// }

// pub struct SelectSender<T>
// {
//     sender: Sender<T>,
//     selection_sender: Sender<usize>,
//     index: usize,
// }
