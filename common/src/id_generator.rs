// std lib
use std::collections::binary_heap::BinaryHeap;
use std::cmp::{Ord};
use std::default::Default;

/// Linear id generator using u32 ints.
/// Keeps track of freed/unused ids, using them on new id requests to fill in holes
/// with the intention of maintaining better cache locality.
pub struct IdGenerator<T: Ord + Default>
{
    counter: T,
    unused: BinaryHeap<T>
}

////////// Implementation //////////

impl<T: Ord + Default> IdGenerator<T>
{
    pub fn new() -> IdGenerator<T>
    {
        IdGenerator { counter: T::default(), unused: BinaryHeap::new() }
    }

    pub fn release_id(&mut self, id: T)
    {
        self.unused.push(id);
    }
}

impl IdGenerator<usize>
{
    pub fn get_id(&mut self) -> usize
    {
        let popped = self.unused.pop();
        match popped
        {
            None =>
            {
                let id = self.counter;
                self.counter += 1;

                id
            },
            Some(id) => { id }
        }
    }
}

impl IdGenerator<u32>
{
    pub fn get_id(&mut self) -> u32
    {
        let popped = self.unused.pop();
        match popped
        {
            None =>
            {
                let id = self.counter;
                self.counter += 1;

                id
            },
            Some(id) => { id }
        }
    }
}
