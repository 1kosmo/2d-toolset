extern crate time;
use self::time::{Timespec, Duration};

use std;

pub struct Timer
{
    start_time: Timespec,

    last_taken: Timespec,
    last_interval: Duration,

    fps_current: u8,
    time_to_sleep: Duration,
    ideal_sleep: Duration,
}

impl Timer
{
    /// Initialize a timer that will sleep or output sleep times according to the
    /// given target FPS.
    pub fn new(target_fps: u8) -> Timer
    {
        Timer
        {
            fps_current: 0,
            start_time: time::get_time(),
            last_taken: time::get_time(),
            last_interval: Duration::zero(),
            time_to_sleep: Duration::milliseconds(
                1_000 / ((target_fps as i64) - 1)),
            ideal_sleep: Duration::milliseconds(
                1_000 / (target_fps as i64)),
        }
    }

    /// Update Timer and get the recommended time to sleep according according to
    /// timer calculations to be within the target FPS.
    pub fn tick_and_get_time(&mut self) -> Duration
    {
        let cur: Timespec = time::get_time();
        self.last_interval = self.last_taken - cur;

        if self.last_taken.sec < cur.sec
        {
            // println!("Got {} frames", self.fps_current);
            self.fps_current = 1;
        }
        else
        {
            self.fps_current = self.fps_current + 1;
        }

        let time_to_sleep =
            if self.ideal_sleep.num_milliseconds() * (self.fps_current as i64) <
            (( cur.nsec as i64) / 1_000_000)
        {
            self.time_to_sleep = self.time_to_sleep / 2;
            self.time_to_sleep
        }
        else
        {
            self.ideal_sleep
        };

        self.last_taken = cur;
        return time_to_sleep
    }

    /// Update Timer and sleep according to the target FPS of the timer.
    pub fn tick_and_sleep(&mut self)
    {
        std::thread::sleep(self.tick_and_get_time().to_std().unwrap());
    }

    /// Get the last calculated sleep interval.
    pub fn interval(&self) -> Duration
    {
        self.last_interval
    }

    /// Get total time since timer started running.
    pub fn get_total_time(&self) -> Duration
    {
        return self.start_time - self.last_taken;
    }
}
