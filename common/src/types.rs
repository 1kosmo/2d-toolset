pub mod object_manipulator
{
    // ID types
    pub type ObjectId = usize;
    pub type FilterId = u32;
    pub type ViewportId = u32;

    pub const NULL_OBJECT_ID: ObjectId = 0;
    pub const NULL_FILTER_ID: FilterId = 0;
    pub const NULL_VIEWPORT_ID: ViewportId = 0;
}

pub mod renderer
{
    extern crate cgmath;

    // ID types
    pub type TextureId = usize;
 
    pub const NULL_TEXTURE_ID: TextureId = 0;

    // Other types
    pub type Viewport = cgmath::Matrix4<f32>;
    pub struct RenderableObject
    {
        pub shape: cgmath::Matrix4<f32>,
        pub texture: TextureId,
    }
    pub type RenderFrame = Vec<(Viewport, Vec<RenderableObject>)>;
}
