////////// std libs //////////
use std::vec::Vec;

////////// My libs //////////
use object_manipulator::object_collection::ObjectId;
use ElementId;

mod window;
mod button;
// mod scroll_bar;

use self::window::Window;
use self::button::Button;

pub enum Element
{
    Window(Window),
    Button(Button),
}

struct BaseElement
{
    objects: Vec<ObjectId>,
    child_elements: Vec<ElementId>,
}
