////////// std libs //////////
use std::sync::mpsc::{channel, Receiver, Sender};
use std::vec::Vec;

////////// My Libs //////////
extern crate common;
extern crate object_manipulator;

use common::id_generator::IdGenerator;
use object_manipulator::external_api::Message;

mod elements;
use elements::Element;

pub type ElementId = usize;

pub struct Kui
{
    id_gen: IdGenerator<ElementId>,

    elements: Vec<Element>,

    // Channels to communicate with ObjectCollection
    from_object_manipulator: Receiver<Message>,
    to_object_manipulator: Sender<Vec<Message>>,
}
