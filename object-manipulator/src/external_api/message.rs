// std lib
use std::hash::{Hash, Hasher};
// My libs
use common::types::object_manipulator::*;
use common::types::renderer::TextureId;

/// A wrapper for the MessageContent enum used for message hashing based on
/// filter_id, in order to split the messages outgoing to listeners.
#[derive(Clone, Copy)]
pub struct Message
{
    pub filter_id: FilterId,
    pub content: MessageContent
}

impl Message
{
    pub fn new(content: MessageContent) -> Message
    {
        Message { filter_id: NULL_FILTER_ID, content: content }
    }

    pub fn new_filtered(filter: FilterId, content: MessageContent) -> Message
    {
        Message { filter_id: filter, content: content }
    }
}

/// Object Manipulator MessageContent format describing manipulation to be done to
/// an object with given id, or what was done to an object if info is coming from
/// the Manipulator.
#[derive(Clone, Copy)]
pub enum MessageContent
{
    CreateNullObject
    {
        object_id: ObjectId,
        parent_id: ObjectId,
        texture_id: TextureId,
    },
    DeleteObject
    {
        object_id: ObjectId
    },    
    /// IsObjectClicked queries whether an object was clicked or not, causing
    /// the object manipulator to send a corresponding ObjectClicked message.
    /// x/y location is given in terms of OpenGL clipspace, i.e. (-1.0,-1.0)
    /// is the bottom-left corner, and (1.0, 1.0) is the top-right.
    IsObjectClicked
    {
        x: f32,
        y: f32,
    },
    /// IsObjectClickedRecursive includes the children of an object in calculating
    /// whether the object was clicked or not.
    IsObjectClickedRecursive
    {
        x: f32,
        y: f32,
    },
    /// ObjectClicked returns the ID of the clicked object, with the x/y location
    /// relative to the center and basis of object of where it was clicked.
    ObjectClicked
    {
        object_id: ObjectId,
        x: f32,
        y: f32,
    },
    MoveObject
    {
        object_id: ObjectId,
        x: f32,
        y: f32,
    },
    RotateObject
    {
        object_id: ObjectId,
        rad: f32,
    },
    ScaleObject
    {
        object_id: ObjectId,
        x: f32,
        y: f32,
    },
    SetObjectPosition
    {
        object_id: ObjectId,
        x: f32,
        y: f32,
    },
    SetObjectRotation
    {
        object_id: ObjectId,
        rad: f32,
    },
    SetObjectSize
    {
        object_id: ObjectId,
        x: f32,
        y: f32,
    },
    SetViewportPosition
    {
        viewport: ViewportId,
        x: f32,
        y: f32,
    },
    SetViewportDimensions
    {
        viewport: ViewportId,
        width: f32,
        height: f32
    },
    SetViewportNearFar
    {
        viewport: ViewportId,
        near: f32,
        far: f32
    },
    CreateViewport
    {
        viewport: ViewportId
    },
    CreateFilter{},
    DeleteFilter{},
    SetFilterVisible{},
    SetFilterInvisible{},
    LinkFilterToViewport
    {
        viewport: ViewportId,
    },

    // MoveObjectZ
    // {
    // object_id: ObjectId,
    //     z: f32,
    // },
    // SetObjectParent
    // {
    // object_id: ObjectId,
    //     parent_id: ObjectId,
    // }

    // DefineAnimation
    // // Idea is to ha,ve a message of this type be leading in
    // // message pack, thus making the actions in a pack an animation
    // {
    //     id: AnimationId
    // }, 
    // UseAnimation
    // {
    //     id: AnimationId
    // },
}

impl PartialEq for Message
{
    fn eq(&self, other: &Message) -> bool
    {
        self.filter_id == other.filter_id &&
            self.content == other.content
    }
}

impl Eq for Message
{
}

impl Hash for Message
{
    fn hash<H>(&self, state: &mut H) where H: Hasher
    {
        Hash::hash(&self.filter_id, state);
    }
}


impl PartialEq for MessageContent
{
    fn eq(&self, other: &MessageContent) -> bool
    {
        use self::MessageContent::*;

        match (*self, *other)
        {
            (ObjectClicked{ .. }, ObjectClicked{ .. }) => true,
            (IsObjectClicked{ .. }, IsObjectClicked{ .. }) => true,
            (MoveObject{ .. }, MoveObject{ .. }) => true,
            (RotateObject{ .. }, RotateObject{ .. }) => true,
            (ScaleObject{ .. }, ScaleObject{ .. }) => true,
            // (CreateObject{ .. }, CreateObject{ .. }) => true,
            (DeleteObject{ .. }, DeleteObject{ .. }) => true,
            (SetObjectSize{ .. }, SetObjectSize{ .. }) => true,
            (SetObjectPosition{ .. }, SetObjectPosition{ .. }) => true,
            (SetObjectRotation{ .. }, SetObjectRotation{ .. }) => true,
            (CreateNullObject{ .. }, CreateNullObject{ .. }) => true,
            (IsObjectClickedRecursive{ .. }, IsObjectClickedRecursive{ .. }) => true,
            (SetViewportPosition{ .. }, SetViewportPosition{ .. }) => true,
            (SetViewportDimensions{ .. }, SetViewportDimensions{ .. }) => true,
            (SetViewportNearFar{ .. }, SetViewportNearFar{ .. }) => true,
            (CreateViewport{ .. }, CreateViewport{ .. }) => true,
            (CreateFilter{ .. }, CreateFilter{ .. }) => true,
            (DeleteFilter{ .. }, DeleteFilter{ .. }) => true,
            (SetFilterVisible{ .. }, SetFilterVisible{ .. }) => true,
            (SetFilterInvisible{ .. }, SetFilterInvisible{ .. }) => true,
            (LinkFilterToViewport{ .. }, LinkFilterToViewport{ .. }) => true,
            _ => false,
        }
    }
}

impl Eq for MessageContent
{
}

impl Hash for MessageContent
{
    fn hash<H>(&self, state: &mut H) where H: Hasher
    {
        use self::MessageContent::*;
        match *self
        {
            ObjectClicked{ .. } => state.write_u32(0),
            MoveObject{ .. } => state.write_u32(1),
            ScaleObject{ .. } => state.write_u32(2),
            RotateObject{ .. } => state.write_u32(3),
            // CreateObject{ .. } => state.write_u32(4),
            DeleteObject{ .. } => state.write_u32(5),
            SetObjectSize{ .. } => state.write_u32(6),
            SetObjectPosition{ .. } => state.write_u32(7),
            SetObjectRotation{ .. } => state.write_u32(8),
            CreateNullObject{ .. } => state.write_u32(9),
            IsObjectClicked{ .. } => state.write_u32(10),
            IsObjectClickedRecursive{ .. } => state.write_u32(11),
            SetViewportPosition{ .. } => state.write_u32(12),
            SetViewportDimensions{ .. } => state.write_u32(13),
            SetViewportNearFar{ .. } => state.write_u32(14),
            CreateViewport{ .. } => state.write_u32(15),
            CreateFilter{ .. } => state.write_u32(16),
            DeleteFilter{ .. } => state.write_u32(17),
            SetFilterVisible{ .. } => state.write_u32(18),
            SetFilterInvisible{ .. } => state.write_u32(19),
            LinkFilterToViewport{ .. } => state.write_u32(20),
        }
    }
}
