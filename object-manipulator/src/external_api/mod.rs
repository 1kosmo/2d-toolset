// std lib
use std::sync::mpsc::{Sender};
use std::sync::{Arc, Mutex};
use std::ops::DerefMut;
use std::vec::Vec;

// 3rd party libs
// use cgmath::Matrix4;

// My libs
pub mod message;

use common::id_generator::IdGenerator;
use self::message::{Message, MessageContent};
use common::types::object_manipulator::*;
use common::types::renderer::TextureId;

#[derive(Clone)]
/// ObjectManipulatorInterface contains helper methods for making Messages to be
/// sent to the ObjectManipulator via a channel. It encapsulates unique-id
/// generation and id reclamation; other Messages can be hand made however.
pub struct ObjectManipulatorInterface
{
    control_channel: Sender<Vec<Message>>,
    id_generator: Arc<Mutex<IdGenerator<ObjectId>>>,
    filter_id_generator: Arc<Mutex<IdGenerator<FilterId>>>,
    viewport_id_generator: Arc<Mutex<IdGenerator<ViewportId>>>,
}

impl ObjectManipulatorInterface
{
    pub(crate) fn new(channel: Sender<Vec<Message>>,
                      id_generator: Arc<Mutex<IdGenerator<ObjectId>>>,
                      filter_id_generator: Arc<Mutex<IdGenerator<FilterId>>>,
                      viewport_id_generator: Arc<Mutex<IdGenerator<ViewportId>>>)
                      -> ObjectManipulatorInterface
    {
        ObjectManipulatorInterface
        {
            control_channel: channel,
            id_generator: id_generator,
            filter_id_generator: filter_id_generator,
            viewport_id_generator: viewport_id_generator,
        }
    }

    /// Sends a batch of messages to the ObjectManipulator via a channel.
    pub fn send_messages(&self, msg_batch: Vec<Message>)
    {
        self.control_channel.send(msg_batch).unwrap();
    }

    /// Creates message representing request to create a null-initialized
    /// object with the given IDs. 
    pub fn create_null_object(&self, parent_id: ObjectId,
                              texture_id: TextureId,
                              filter_id: FilterId)
                              -> (Message, ObjectId)
    {
        let id = self.id_generator.lock().unwrap().deref_mut().get_id();

        let msg = Message::new_filtered(
            filter_id,
            MessageContent::CreateNullObject
            {
                object_id: id, texture_id: texture_id,
                parent_id: parent_id,
            });
        (msg, id)
    }

    /// Creates message representing a command to clear data for the object.
    /// As a side effect, creating the DeleteObject Message through this function
    /// releases the id in the interface to be used for future Objects.
    pub fn delete_object(&self, id: ObjectId) -> Message
    {
        self.id_generator.lock().unwrap().deref_mut().release_id(id);

        Message::new(MessageContent::DeleteObject{ object_id: id })
    }

    /// Creates message representing request to check if any object matching the
    /// given filter_id has been clicked at the given screenspace(x, y) location.
    pub fn is_object_clicked(&self, x: f32, y: f32, filter_id: FilterId) -> Message
    {
        Message::new_filtered(
            filter_id, MessageContent::IsObjectClicked{x: x, y: y})
    }

    /// Creates message representing request to check if any object matching the
    /// given filter_id has been clicked at the given screenspace(x, y) location.
    pub fn is_object_clicked_recursive(&self, x: f32, y: f32, filter_id: FilterId)
                                       -> Message
    {
        Message::new_filtered(
            filter_id, MessageContent::IsObjectClickedRecursive{x: x, y: y})
    }

    /// Creates message representing request to move the object with the given id
    /// by (x, y) screenspace coordinates.
    /// Considering this queries objects by screenspace coordinates, it takes into
    /// account the view-space dimensions of the filter that this object belongs to.
    pub fn move_object(&self, id: ObjectId, x: f32, y: f32) -> Message
    {
        Message::new(MessageContent::MoveObject{ object_id: id, x: x, y: y })
    }

    /// Creates message representing request to rotate the object.
    pub fn rotate_object(&self, id: ObjectId, rad: f32) -> Message
    {
        Message::new(MessageContent::RotateObject{ object_id: id, rad: rad })
    }

    /// Creates message representing request to linearly scale the object in the
    /// objects's model frame.
    pub fn scale_object(&self, id: ObjectId, x: f32, y: f32) -> Message
    {
        Message::new(MessageContent::ScaleObject{ object_id: id, x: x, y: y })
    }

    /// Creates message representing request to set the absolute position of the
    /// object in the ObjectManipulator's world space to (x,y)
    pub fn set_object_position(&self, id: ObjectId, x: f32, y: f32) -> Message
    {
        Message::new(MessageContent::SetObjectPosition{ object_id: id, x: x, y: y })
    }

    /// Creates message representing request to set the absolute rotation of the
    /// object in its model space.
    pub fn set_object_rotation(&self, id: ObjectId, rad: f32) -> Message
    {
        Message::new(MessageContent::SetObjectRotation{ object_id: id, rad: rad })
    }

    /// Creates message representing request to set the absolute (x, y) dimensions
    /// of the object.
    pub fn set_object_size(&self, id: ObjectId, x: f32, y: f32) -> Message
    {
        Message::new(MessageContent::SetObjectSize{ object_id: id, x: x, y: y })
    }

    pub fn set_viewport_position(&self, viewport: ViewportId, x: f32, y: f32)
                                 -> Message
    {
        Message::new(MessageContent::SetViewportPosition{
            viewport: viewport, x: x, y: y })
    }

    pub fn set_viewport_dimensions(&self, viewport: ViewportId,
                                   width: f32, height: f32) -> Message
    {
        Message::new(MessageContent::SetViewportDimensions{
            viewport: viewport, width: width, height: height })
    }

    pub fn set_viewport_nearfar(&self, viewport: ViewportId, near: f32, far: f32)
                                -> Message
    {
        Message::new(MessageContent::SetViewportNearFar{
            viewport: viewport, near: near, far: far })
    }

    pub fn create_viewport(&self) -> (Message, ViewportId)
    {
        let id = self.viewport_id_generator.lock().unwrap().deref_mut().get_id();

        let msg = Message::new(MessageContent::CreateViewport{ viewport: id });
        (msg, id)
    }

    pub fn create_filter(&self) -> (Message, FilterId)
    {
        let id = self.filter_id_generator.lock().unwrap().deref_mut().get_id();

        let msg = Message::new_filtered(id, MessageContent::CreateFilter{});
        (msg, id)
    }

    pub fn delete_filter(&self, filter: FilterId) -> Message
    {
        Message::new_filtered(filter, MessageContent::DeleteFilter{})
    }

    pub fn set_filter_visible(&self, filter: FilterId) -> Message
    {
        Message::new_filtered(filter, MessageContent::SetFilterVisible{})
    }

    pub fn set_filter_invisible(&self, filter: FilterId) -> Message
    {
        Message::new_filtered(filter, MessageContent::SetFilterInvisible{})
    }

    pub fn link_filter_to_viewport(&self, filter: FilterId, viewport: ViewportId)
                                   -> Message
    {
        Message::new_filtered(filter,
                              MessageContent::LinkFilterToViewport{
                                  viewport: viewport })
    }
}
