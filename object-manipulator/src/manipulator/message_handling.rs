////////// std lib //////////
use std::mem;
use std::collections::{HashMap, hash_map};
////////// 3rd Party //////////
use cgmath::{Matrix, Point2, Vector2, Vector4, Matrix4};
////////// My libs //////////
use manipulator::*;
use manipulator::misc::*;
use external_api::message::{Message, MessageContent};
use object::{RawObject, Object, intersection};
use object::aabb::AABB;

impl ObjectManipulator
{
    /// Executes actions necessary to handle a given message from the control
    /// channel. 
    pub(super) fn handle_message(&mut self, messages: Vec<Message>)
    {
        for msg in messages
        {
            match msg.content
            {                
                MessageContent::CreateNullObject
                {
                    object_id, parent_id, texture_id
                } => {
                    self.create_null_object(object_id, parent_id, texture_id,
                                            msg.filter_id);
                },
                MessageContent::DeleteObject{ object_id } =>
                {
                    self.delete_object(object_id);
                },
                MessageContent::IsObjectClicked{ x, y } =>
                {
                    self.get_clicked_objects(msg.filter_id, x, y);
                },
                MessageContent::IsObjectClickedRecursive{ x, y } =>
                {
                    self.get_clicked_objects_recursive(msg.filter_id, x, y);
                },
                MessageContent::ObjectClicked { .. } => 
                {
                },
                MessageContent::MoveObject { object_id, x, y } => 
                {
                    self.raws[object_id].translate(x, y);
                },
                MessageContent::RotateObject { object_id, rad } => 
                {
                    self.raws[object_id].rotate(rad);
                },
                MessageContent::ScaleObject { object_id, x, y } => 
                {
                    self.raws[object_id].scale(x, y);
                }
                MessageContent::SetObjectPosition { object_id, x, y } => 
                {
                    self.raws[object_id].set_position(x, y);
                },
                MessageContent::SetObjectRotation { object_id, rad } => 
                {
                    self.raws[object_id].set_rotation(rad);
                },
                MessageContent::SetObjectSize { object_id, x, y } => 
                {
                    self.raws[object_id].set_size(x, y);
                },
                MessageContent::SetViewportPosition { viewport, x, y } =>
                {
                    self.set_viewport_position(viewport, x, y);
                },
                MessageContent::SetViewportDimensions { viewport, width, height } =>
                {
                    self.set_viewport_dimensions(viewport, width, height);
                },
                MessageContent::SetViewportNearFar { viewport, near, far } =>
                {
                    self.set_viewport_nearfar(viewport, near, far);
                },
                MessageContent::CreateViewport { viewport } =>
                {
                    // Default Viewport is :
                    // Right = 50, Left = -50,
                    // Top = 50, Bottom = -50,
                    // Far = 1, Near = -1
                    // Pos(0.0, 0.0)
                    self.viewports.insert(viewport,
                                          ViewportInfo::new(50.0, 50.0, -1.0, 1.0));
                },
                MessageContent::CreateFilter{} =>
                {
                    self.initialize_filter(&msg.filter_id);
                },
                MessageContent::DeleteFilter{} =>
                {
                    self.delete_filter(&msg.filter_id);
                },
                MessageContent::SetFilterVisible{} =>
                {
                    self.set_filter_visible(msg.filter_id);
                },
                MessageContent::SetFilterInvisible{} =>
                {
                    self.set_filter_invisible(msg.filter_id);
                },
                MessageContent::LinkFilterToViewport { viewport } =>
                {
                    self.link_filter_to_viewport(msg.filter_id, viewport);
                },
            } // End match statement
            
        } // End for loop
    }

    fn create_null_object(&mut self, id: ObjectId, parent_id: ObjectId,
                          texture_id: TextureId, filter_id: FilterId)
    {
        // Expand capacity of all container vectors if current size is too small
        if self.raws.len() < id
        {
            self.expand_buffers();
        }

        self.parents[id] = parent_id;
        self.child_lists[parent_id].push(id);
        self.textures[id] = texture_id;
        self.apply_object_to_filter(id, &filter_id);
    }

    /// Returns true if the specified point intersects with the object.
    /// Because the object is specified in the viewport-space it's tied to,
    /// the coordinates must therefore also be specified in viewport-space.
    fn is_point_over_object(
        &self,
        inverse_transform_mats: &mut HashMap<ObjectId, Matrix4<f32>>,
        object: ObjectId, coordinates: &Point2<f32>)
        -> bool
    {
        let parent = self.parents[object];

        if parent == NULL_OBJECT_ID
        {
            intersection::test_point_obb(&self.raws[object], coordinates)
        }
        else
        {
            let transpose = match inverse_transform_mats.entry(parent)
            {
                hash_map::Entry::Occupied(v) => { v.into_mut() },
                hash_map::Entry::Vacant(v) =>
                {
                    v.insert(self.finals[parent]
                             .child_propagation_form.transpose())
                }
            };

            let click_pos = Vector4::new(coordinates.x, coordinates.y, 0.0, 1.0);
            let click_pos = *transpose * click_pos;
            intersection::test_point_obb(
                &self.raws[object], &Point2::new(click_pos.x, click_pos.y))
        }
    }

    /// Uses the viewport bound to the specified filter to transform coordinates
    /// given in clipspace to ones in terms of the viewport.
    fn clipspace_to_viewport_coordinates(&mut self, filter: FilterId,
                                         x: f32, y: f32) -> Point2<f32>
    {
        let viewport = self.viewports.get(
            self.filter_viewport_mapping.get(&filter).unwrap()
        ).expect(format!("ERROR: [OM] clipspace_to_viewport_coordinates - Failed to find the viewport bound to filter={}", filter).as_str());
        Point2::new(x * viewport.halfwidth, y * viewport.halfheight)
    }

    /// Identifies if any objects were intersecting with a point specified by (x,y),
    /// and sends messages to appropriate channels notifying which objects were
    /// intersecting.
    fn get_clicked_objects(&mut self, filter: FilterId, x: f32, y: f32)
    {
        let mut inverse_transform_mats = HashMap::new();
        let mut messages_to_send = Vec::new();

        // We want to express the (x,y) clip-space coordinates as coordinates
        // of the viewport used by the filter.
        let coordinates = self.clipspace_to_viewport_coordinates(filter, x, y);

        for object in &self.filter_mapping[&filter]
        {
            if self.is_point_over_object(&mut inverse_transform_mats, *object,
                                         &coordinates)
            {
                // TODO: Implement returning correct x/y coordinates of clicked obj
                messages_to_send.push(
                    Message::new_filtered(
                        filter,
                        MessageContent::ObjectClicked {
                            object_id: *object, x: 0.0, y: 0.0 }
                    ));
            }
        } // End of for loop

        self.send_messages(messages_to_send);
    }

    /// For now only uses the bounding volume in order to determine if object was
    /// clicked or not. Recursive because bounding volumes bound 
    fn get_clicked_objects_recursive(&mut self, filter: FilterId, x: f32, y: f32)
    {
        let mut messages_to_send = Vec::new();

        // We want to express the (x,y) clip-space coordinates as coordinates
        // of the viewport used by the filter.
        let coordinates = self.clipspace_to_viewport_coordinates(filter, x, y);

        for object in &self.filter_mapping[&filter]
        {
            // Check if bounding box object for the entire hierarchy chain
            // is intersecting with point.
            if intersection::test_point_aabb(
                self.bounding_volumes.get(*object).unwrap(), &coordinates)
            {
                // TODO: Implement checking actual object boundaries instead of just
                // checking bounding volumes 
                messages_to_send.push(Message::new_filtered(
                    filter,
                    MessageContent::ObjectClicked {
                        object_id: *object, x: 0.0, y: 0.0}
                ));
            }
        }
    }

    /// Initializes all values associated to the object specified by id to
    /// defaults, appending the children to the object to the object's parent
    fn delete_object(&mut self, id: ObjectId)
    {        
        self.raws[id] = RawObject::new_empty();
        self.z_levels[id] = DEFAULT_Z_LEVEL;
        self.bounding_volumes[id] = AABB::new_empty();
        self.textures[id] = NULL_TEXTURE_ID;
        self.finals[id] = Object::new_empty();

        // Append owned children to parent,
        // this operation leaves the child_list[id] empty
        let mut children = mem::replace(&mut self.child_lists[id], Vec::new());
        self.child_lists[self.parents[id]].append(&mut children);
        self.parents[id] = NULL_OBJECT_ID;
    }

    /// Same as delete_object, except all children of the object are also deleted
    fn delete_object_recursive(&mut self, id: ObjectId)
    {
        self.raws[id] = RawObject::new_empty();
        self.z_levels[id] = DEFAULT_Z_LEVEL;
        self.bounding_volumes[id] = AABB::new_empty();
        self.textures[id] = NULL_TEXTURE_ID;
        self.finals[id] = Object::new_empty();
        self.parents[id] = NULL_OBJECT_ID;
        let children = mem::replace(&mut self.child_lists[id], Vec::new());
        // Recursively delete all children
        for child in children
        {
            self.delete_object_recursive(child);
        }
    }

    /// Sets the worldspace viewport position.
    fn set_viewport_position(&mut self, viewport: ViewportId, x: f32, y: f32)
    {
        let viewport = self.viewports.get_mut(&viewport).unwrap();
        viewport.pos = Vector2::new(x, y);
        viewport.update_mat_and_aabb();
    }

    /// Sets the width/height values of the orthographic projection used for the
    /// viewport.
    fn set_viewport_dimensions(&mut self, viewport: ViewportId,
                               width: f32, height: f32)
    {
        let viewport = self.viewports.get_mut(&viewport).unwrap();
        viewport.halfwidth = width / 2.0;
        viewport.halfheight = height / 2.0;
        viewport.update_mat_and_aabb();
    }

    /// Sets the near/far values of the orthographic projection used for the
    /// viewport.
    fn set_viewport_nearfar(&mut self, viewport: ViewportId, near: f32, far: f32)
    {
        let viewport = self.viewports.get_mut(&viewport).unwrap();
        viewport.near = near;
        viewport.far = far;
        viewport.update_mat_and_aabb();      
    }

    /// Places default values associated with a new filter. These are:
    ///  - Vec mapping ObjectIds to the specified filter
    ///  - Filter visibility, which is visible by default
    ///  - Default ViewportId assigned to the specified filter
    /// The caller of this function is the one responsible for making sure that an
    /// already-initialized filter does not get overwritten by this function.
    fn initialize_filter(&mut self, filter: &FilterId)
    {
        self.filter_mapping.insert(*filter, Vec::new());
        self.filter_visibility.insert(*filter, true);
        self.filter_viewport_mapping.insert(*filter, NULL_VIEWPORT_ID);
    }

    /// Deletes all entries associated to the filter, but also deletes performs a
    /// recursive delete on all objects associated with this filter, meaning even
    /// objects unassociated with this filter might be deleted.
    fn delete_filter(&mut self, filter: &FilterId)
    {
        self.filter_viewport_mapping.remove(filter);
        if let Some(objects) = self.filter_mapping.remove(filter)
        {
            for object in objects
            {
                self.delete_object_recursive(object);
            }
        }
    }

    fn set_filter_visible(&mut self, filter: FilterId)
    {
        if let Some(prev_status) = self.filter_visibility.insert(filter, true)
        {
            debug!("DEBUG: [OM] Set filter visibility from {} to {}",
                   prev_status, true);
        }
    }

    fn set_filter_invisible(&mut self, filter: FilterId)
    {
        if let Some(prev_status) = self.filter_visibility.insert(filter, false)
        {
            debug!("DEBUG: [OM] Set filter visibility from {} to {}",
                   prev_status, false);
        }
    }


    fn link_filter_to_viewport(&mut self, filter: FilterId, viewport: ViewportId)
    {
        if !self.viewports.contains_key(&viewport)
        {
            println!("ERROR: [OM] Attempted to link filter to an invalid viewport.");
        }
        if let Some(previous_viewport) = self.filter_viewport_mapping.insert(
            filter, viewport)
        {
            debug!("DEBUG: [OM] Filter {} reassigned viewport: {} => {}",
                   filter, previous_viewport, viewport);
        }
    }

    /// Sends a given vector of Messages across the appropriate channel
    fn send_messages(&mut self, messages: Vec<Message>)
    {
        for msg in messages
        {
            self.general_event_queue.send(msg).unwrap();
        }
    }
}
