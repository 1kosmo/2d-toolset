////////// std lib //////////

////////// 3rd Party //////////
use cgmath;
use cgmath::{Zero, Vector2, Point2, Matrix4};
////////// My libs //////////
use manipulator::*;
use object::aabb::AABB;

impl ObjectManipulator
{
    /// Doubles the buffers storing object details, filling the vectors with default
    /// values.
    pub(crate) fn expand_buffers(&mut self)
    {
        let new_size = self.raws.len() * 2;
        
        // Populate the vectors by default values: most are unusable in this
        // state, like the RawObjects and final Objects.
        self.raws.resize(new_size, RawObject::new_empty());
        self.z_levels.resize(new_size, DEFAULT_Z_LEVEL);
        self.child_lists.resize(new_size, Vec::new());
        self.parents.resize(new_size, NULL_OBJECT_ID);
        self.bounding_volumes.resize(new_size, AABB::new_empty());
        self.textures.resize(new_size, NULL_TEXTURE_ID);
        self.finals.resize(new_size, Object::new_empty());
    }

    /// Puts specified object in the filter list, and creates filter list if it does
    /// not yet exist.
    pub(crate) fn apply_object_to_filter(&mut self, object: ObjectId, filter: &FilterId)
    {
        // Add the object to the filter list
        self.filter_mapping.get_mut(filter).expect(
            format!("ERROR: [OM] Applying object={} to invalid filter={}",
                    object, *filter).as_str()
        ).push(object);
    }
}

//// Custom types ////

/// Type used to keep track of data related to a viewport.
pub(crate) struct ViewportInfo
{    
    pub halfwidth: f32,
    pub halfheight: f32,
    pub near: f32,
    pub far: f32,
    pub pos: Vector2<f32>,

    pub mat: Matrix4<f32>,
    pub aabb: AABB,
}

impl ViewportInfo
{
    /// Default constructor that creates a viewport at position (0, 0).
    pub fn new(halfwidth: f32, halfheight: f32, near: f32, far: f32)
               -> ViewportInfo
    {
        let mut viewport = ViewportInfo
        {
            halfwidth: halfwidth,
            halfheight: halfheight,
            near: near,
            far: far,
            pos: Vector2::new(0.0, 0.0),
            mat: Matrix4::zero(),
            aabb: AABB::new_empty(),
        };
        viewport.update_mat_and_aabb();
        viewport
    }

    /// Convenience function to be called after any member of ViewportInfo is
    /// set to a new value, in order to compute the new projection matrix and
    /// bounding box.
    pub fn update_mat_and_aabb(&mut self)
    {
        self.aabb = AABB
        {
            min: Point2::new(-self.halfwidth, -self.halfheight) + self.pos,
            max: Point2::new(self.halfwidth, self.halfheight) + self.pos,
        };
        self.mat = cgmath::ortho(self.aabb.min.x, self.aabb.max.x,
                                 self.aabb.min.y, self.aabb.max.y,
                                 self.near, self.far);
    }
}
