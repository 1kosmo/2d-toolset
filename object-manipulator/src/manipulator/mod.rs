////////// Third party //////////
// use seahash;
// use cgmath::{Matrix4, Vector2};

////////// std lib //////////
use std::vec::Vec;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{Receiver, Sender, channel};

////////// My libs //////////
mod message_handling;
mod misc;

use common::event_queue::EventQueue;
use common::id_generator::IdGenerator;
use object::{Object, RawObject};
use object::aabb::AABB;
use object::intersection;
use external_api::ObjectManipulatorInterface;
use external_api::message::Message;
use common::types::object_manipulator::*;
use common::types::renderer::*;
use self::misc::ViewportInfo;

const DEFAULT_Z_LEVEL: f32 = 0.0;

pub struct ObjectManipulator
{
    //// ID generators ////
    _id_gen: Arc<Mutex<IdGenerator<ObjectId>>>,
    _filter_gen: Arc<Mutex<IdGenerator<FilterId>>>,
    _viewport_gen: Arc<Mutex<IdGenerator<ViewportId>>>,

    //// Data indexed by ObjectId ////
    // Raw physical data type for simple manipulation
    raws: Vec<RawObject>,
    // Matrix-form objects computed after finalizing updates
    finals: Vec<Object>,
    // Misc. data
    z_levels: Vec<f32>,
    child_lists: Vec<Vec<ObjectId>>,
    parents: Vec<ObjectId>,
    bounding_volumes: Vec<AABB>,
    textures: Vec<TextureId>, 

    //// Other data ////
    viewports: HashMap<ViewportId, ViewportInfo>,
    filter_mapping: HashMap<FilterId, Vec<ObjectId>>,
    filter_visibility: HashMap<FilterId, bool>,
    filter_viewport_mapping: HashMap<FilterId, ViewportId>,
    updated_objects: HashSet<ObjectId>,

    control_channel: Receiver<Vec<Message>>,
    to_renderer: Sender<RenderFrame>,
    general_event_queue: Sender<Message>,
}

////////// Implementation //////////

impl ObjectManipulator
{
    /// Default constructor for ObjectManipulator.
    /// Returns the actual manipulator, the interface used for sending messages to
    /// it and generating IDs, and the EventQueue the manipulator uses to
    /// communicate events to others (e.g. ObjectClicked, etc.).
    pub fn new(to_renderer: Sender<RenderFrame>) ->
        (ObjectManipulator, ObjectManipulatorInterface, EventQueue<Message>)
    {
        let id_generator = Arc::new(Mutex::new(IdGenerator::new()));
        let filter_gen = Arc::new(Mutex::new(IdGenerator::new()));
        let viewport_gen = Arc::new(Mutex::new(IdGenerator::new()));

        let (ctrl_sender, ctrl_receiver) = channel();
        let interface = ObjectManipulatorInterface::new(
            ctrl_sender,
            id_generator.clone(), filter_gen.clone(), viewport_gen.clone());
        let event_queue = EventQueue::new(); 

        let manipulator = ObjectManipulator
        {
            _id_gen: id_generator,
            _filter_gen: filter_gen,
            _viewport_gen: viewport_gen,

            raws: Vec::new(),
            finals: Vec::new(),
            z_levels: Vec::new(),
            child_lists: Vec::new(),
            parents: Vec::new(),
            bounding_volumes: Vec::new(),
            textures: Vec::new(),

            viewports: HashMap::new(),
            filter_mapping: HashMap::new(),
            filter_visibility: HashMap::new(),
            filter_viewport_mapping: HashMap::new(),
            updated_objects: HashSet::new(),

            control_channel: ctrl_receiver,
            to_renderer: to_renderer,
            general_event_queue: event_queue.get_sender(),
        };

        (manipulator, interface, event_queue)
    }

    /// finalize_updates() creates new matrices for every object whose
    /// properties got updated since this function was last called.
    /// Parent-child hierarchies are taken into account, and parent objects
    /// are updated first.
    fn finalize_updates(&mut self)
    {
        for object in self.updated_objects.clone()
        {
            if self.updated_objects.contains(&self.parents[object as usize])
            {
                continue
            }

            self.update_object(object);
        }

        self.updated_objects.clear();
    }

    /// update_object(...) uses the currently available parent transformation
    /// and the object's properties (stored in RawObject class) to create new
    /// matrices for the object (i.e. Object class).
    fn update_object(&mut self, object_id: ObjectId)
    {
        let parent_id = self.parents[object_id];
        // No parent transformation
        if parent_id == 0
        {
            self.finals[object_id] = Object::from_raw(&self.raws[object_id],
                                                      self.z_levels[object_id]);
        }
        // Use parent transformation matrix
        else
        {
            self.finals[object_id] = Object::from_raw_and_parent(
                &self.raws[object_id],
                &self.finals[parent_id],
                self.z_levels[object_id]);
        }
    }

    fn make_render_frame(&self) -> RenderFrame
    {
        let mut frame: RenderFrame = Vec::new();
        for (filter, objects) in &self.filter_mapping
        {
            // If filter invisible, do not consider it in render frame
            if *self.filter_visibility.get(filter).unwrap() == false { continue; }
            
            let viewport = self.viewports.get(filter).unwrap();
            let mut to_render = Vec::new();

            for object in objects
            {
                let texture = self.textures[*object];
                if texture != NULL_TEXTURE_ID
                {
                    // If any part the entire structure under the root-level object
                    // intersects with the viewport, then we will render it.
                    let bounding_box = self.bounding_volumes.get(*object).unwrap();
                    if intersection::test_aabb_aabb(bounding_box, &viewport.aabb)
                    {
                        to_render.push(
                            RenderableObject
                            {
                                shape: self.finals[*object].renderer_form,
                                texture: texture,
                            });
                    } // End if intersection
                } // End if texture
            } // End for

            if to_render.len() > 0 { frame.push((viewport.mat, to_render)); }
        }
        frame
    }

    /// Synchronous single threaded execution of everything the manipulator
    /// needs to do. Handles sent messages sent via the control channel, then
    /// sends updated matrices to the renderer.
    ///
    /// Call once per frame. 
    pub fn run(&mut self)
    {
        loop
        {
            match self.control_channel.try_recv()
            {
                Ok(msg) => { self.handle_message(msg); },
                Err(_) => { break },
            }
            // TODO: Apply updates related to animations

            // Finalize and send updated matrices to the renderer
            self.finalize_updates();
            let render_frame = self.make_render_frame();
            match self.to_renderer.send(render_frame)
            {
                Ok(()) => {},
                Err(send_err) =>
                {
                    use std::error::Error;
                    println!("ERROR: Failed to send frame to renderer ({})",
                             send_err.description());
                }
            }
        }
    }

    /// Asynchronous version of run(). Instead of exiting after consuming all
    /// events, it keeps running waiting for new ones. Also updates animations
    /// based on the set FPS.
    ///
    /// Call from newly created thread once.
    ///
    /// TODO: Add animation handling
    pub fn run_thread(&mut self)
    {
        'main_loop: loop
        {
            // First apply all queued updates
            'msg_processing: loop
            {
                match self.control_channel.try_recv()
                {
                    Ok(msg) => { self.handle_message(msg); },
                    Err(err) =>
                    {
                        use std::sync::mpsc::TryRecvError;
                        match err
                        {
                            TryRecvError::Empty => { break 'msg_processing },
                            TryRecvError::Disconnected => { break 'main_loop }
                        }
                    },
                }
            } // End msg_processing

            // TODO: Apply updates related to animations

            // Finalize and send updated matrices to the renderer
            self.finalize_updates();
            let render_frame = self.make_render_frame();
            match self.to_renderer.send(render_frame)
            {
                Ok(()) => {},
                Err(send_err) =>
                {
                    use std::error::Error;
                    println!("ERROR: Failed to send frame to renderer ({})",
                             send_err.description());
                }
            }
        } // End main_loop
    }
}
