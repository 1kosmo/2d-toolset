////////// 3rd party //////////
use cgmath::{Point2, Vector2};
////////// std lib //////////
use std::f32;
////////// My libs //////////
use super::RawObject;

#[derive(Copy, Clone)]
pub struct AABB
{
    pub min: Point2<f32>,
    pub max: Point2<f32>,
}

impl AABB
{
    pub fn from_raw_object(object: &RawObject) -> AABB
    {
        let mut new = AABB { min: Point2::new(f32::MAX, f32::MAX),
                             max: Point2::new(f32::MIN, f32::MIN) };

        // Calculate the 4 points making up the RawObject
        let x_half = Vector2::new(object.basis.x[0] * object.xy_halfwidths[0],
                                  object.basis.x[1] * object.xy_halfwidths[1]);
        let y_half = Vector2::new(object.basis.y[0] * object.xy_halfwidths[0],
                                  object.basis.y[1] * object.xy_halfwidths[1]);

        let mut points = Vec::new();
        points.reserve_exact(4);
        points.push(object.center + x_half + y_half);
        points.push(object.center + x_half + -y_half);
        points.push(object.center + -x_half + y_half);
        points.push(object.center + -x_half + -y_half);

        for i in 0 .. 4
        {
            for j in 0 .. 2
            {
                new.min[j] = f32::min(new.min[j], points[i][j]);
                new.max[j] = f32::max(new.max[j], points[i][j]);
            }
        }

        new
    }

    pub fn new_empty() -> AABB
    {
        AABB { min: Point2::new(0.0, 0.0), max: Point2::new(0.0, 0.0) }
    }

    pub fn merge_with(&self, other: &AABB) -> AABB
    {
        let mut merged = AABB::new_empty();
        for i in 0 .. 2
        {
            merged.min[i] = f32::min(self.min[i], other.min[i]);
            merged.max[i] = f32::max(self.max[i], other.max[i]);
        }
        merged
    }
}
