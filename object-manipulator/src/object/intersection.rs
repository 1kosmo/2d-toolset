// 3rd party
use cgmath;
use cgmath::{One, Vector2, Matrix3, Point2};

// std libs
use std::{f32};

// My libs
use object::RawObject;
use object::aabb::AABB;

/// Returns true if two given OBBs are intersecting.
/// Implemented according to the separated-axis test show in
/// Real Time Collision Detection.
pub fn test_obb_obb(a: &RawObject, b: &RawObject) -> bool
{
    let mut ra: f32;
    let mut rb: f32;
    let mut rot_mat: Matrix3<f32> = Matrix3::one();
    let mut abs_rot_mat: Matrix3<f32> = Matrix3::one();

    // Rotation matrix expressing b in a's coordinate frame
    for i in 0..2
    {
        for j in 0..2
        {
            rot_mat[i][j] = cgmath::dot(a.basis[i], b.basis[j]);
        }
    }
    rot_mat[2][2] = 1.0;

    let mut translation = b.center - a.center;
    // Bring translation into a's coordinate frame
    translation = Vector2::new(cgmath::dot(translation, a.basis[0]),
                               cgmath::dot(translation, a.basis[1]));

    for i in 0..2
    {
        for j in 0..2
        {
            abs_rot_mat[i][j] = f32::abs(rot_mat[i][j]) + f32::EPSILON;
        }
    }
    abs_rot_mat[2][2] = 1.0;

    for i in 0..2
    {
        ra = a.xy_halfwidths[0];
        rb = b.xy_halfwidths[0] * abs_rot_mat[i][0] +
            b.xy_halfwidths[1] * abs_rot_mat[i][1];
        if f32::abs(translation[i]) > ra + rb { return false; }
    }

    for i in 0..2
    {
        ra = a.xy_halfwidths[0] * abs_rot_mat[0][i] +
            a.xy_halfwidths[1] * abs_rot_mat[1][i];
        rb = b.xy_halfwidths[i];

        if f32::abs(translation[0] * rot_mat[0][i] +
                    translation[1] * rot_mat[1][i])
            > ra + rb
        {
            return false;
        }
    }

    return true;
}

/// Returns true if given point is within bounds of the OBB.
pub fn test_point_obb(obb: &RawObject, point: &Point2<f32>) -> bool
{
    // Idea is to check that point is "to the left" of 4 lines
    let p1 = obb.center + obb.xy_halfwidths;
    let p2 = Point2 { x: obb.center.x + obb.xy_halfwidths.x,
                      y: obb.center.y - obb.xy_halfwidths.y };
    let p3 = obb.center + (-obb.xy_halfwidths);
    let p4 = Point2 { x: obb.center.x - obb.xy_halfwidths.x,
                      y: obb.center.y + obb.xy_halfwidths.y };

    (0.0 < ((point.x - p1.x) * (p2.y - p1.y) -
            (point.y - p1.y) * (p2.x - p1.x))) &&
        (0.0 < ((point.x - p2.x) * (p3.y - p2.y) -
                (point.y - p2.y) * (p3.x - p2.x))) &&
        (0.0 < ((point.x - p3.x) * (p4.y - p3.y) -
                (point.y - p3.y) * (p4.x - p3.x))) &&
        (0.0 < ((point.x - p4.x) * (p1.y - p4.y) -
                (point.y - p4.y) * (p1.x - p4.x)))
}

/// Returns true if given point is within bounds of the AABB.
pub fn test_point_aabb(aabb: &AABB, point: &Point2<f32>) -> bool
{
    let p1 = &aabb.min;
    let p2 = Point2::new(p1.x, aabb.max.y);
    let p3 = &aabb.max;
    let p4 = Point2::new(aabb.max.x, aabb.min.y);

    (0.0 < ((point.x - p1.x) * (p2.y - p1.y) -
            (point.y - p1.y) * (p2.x - p1.x))) &&
        (0.0 < ((point.x - p2.x) * (p3.y - p2.y) -
                (point.y - p2.y) * (p3.x - p2.x))) &&
        (0.0 < ((point.x - p3.x) * (p4.y - p3.y) -
                (point.y - p3.y) * (p4.x - p3.x))) &&
        (0.0 < ((point.x - p4.x) * (p1.y - p4.y) -
                (point.y - p4.y) * (p1.x - p4.x)))
}

/// Returns true if the two AABBs intersect each other.
pub fn test_aabb_aabb(a: &AABB, b: &AABB) -> bool
{
    // Exit with no intersection if separated along an axis
    if a.max[0] < b.min[0] || a.min[0] > b.max[0] { return false }
    if a.max[1] < b.min[1] || a.min[1] > b.max[1] { return false }
    if a.max[2] < b.min[2] || a.min[2] > b.max[2] { return false }
    // Overlapping on all axes means AABBs are intersecting
    return true
}
