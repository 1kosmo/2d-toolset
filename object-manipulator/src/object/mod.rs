// 3rd party
use cgmath::{Vector2, Point2, Vector3, Matrix2, Matrix4};
use cgmath::{Rad, One};

// My libs
pub mod intersection;
pub mod aabb;

#[derive(Copy, Clone)]
pub struct Object
{
    pub renderer_form: Matrix4<f32>,
    pub child_propagation_form: Matrix4<f32>,
}

#[derive(Copy, Clone)]
pub struct RawObject
{
    center: Point2<f32>,
    // xy_axes: [Vector2<f32>; 2],
    basis: Matrix2<f32>,
    xy_halfwidths: Vector2<f32>,
}

////////// Implementation //////////

impl RawObject
{
    /// Constructs a RawObject struct positioned at center_pos with height/width
    /// specified by the parameter halfwidths.
    pub fn new(center_pos: Point2<f32>, halfwidths: Vector2<f32>) -> RawObject
    {
        RawObject { center: center_pos,
                    basis: Matrix2::new(1.0, 0.0, 1.0, 0.0),
                    xy_halfwidths: halfwidths }
    }

    /// Constructs a RawObject struct positioned at center_pos with height/width
    /// specified by halfwidths, and x/y basis specified by basis.
    pub fn new_with_basis(center_pos: Point2<f32>,
                          halfwidths: Vector2<f32>,
                          basis: Matrix2<f32>)
                          -> RawObject
    {
        RawObject { center: center_pos,
                    basis: basis,
                    xy_halfwidths: halfwidths }
    }

    /// Constructs a zero-initialized RawObject 
    pub fn new_empty() -> RawObject
    {
        RawObject
        {
            center: Point2::new(0.0, 0.0),
            basis: Matrix2::new(1.0, 0.0, 1.0, 0.0),
            xy_halfwidths: Vector2::new(0.0, 0.0)
        }
    }
}

impl Object
{
    /// Constructs the transformation matrices using raw data about the object
    /// (RawObject)
    pub fn from_raw(raw: &RawObject, z: f32) -> Object
    {
        let rotation: Matrix4<f32> = raw.basis.into();

        println!("Making sure that a basis transformed into a Mat4 is correct: {:?}",
                 rotation);

        let translation =
            Matrix4::from_translation(Vector3::new(raw.center.x, raw.center.y, z));
        let scale = Matrix4::new(
            raw.xy_halfwidths.x, 0.0, 0.0, 0.0,
            0.0, raw.xy_halfwidths.y, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0);

        let no_scale = translation * rotation;

        Object
        { 
            renderer_form: no_scale * scale,
            child_propagation_form: no_scale,
        }
    }

    /// Constructs the transformation matrices using raw data about the object
    /// (RawObject) and the transformation matrices from a parent Object.
    pub fn from_raw_and_parent(raw: &RawObject, parent: &Object, z: f32) -> Object
    {
        let mut object = Object::from_raw(raw, z);

        // No way to get around doing two matrix multiplications because of the
        // scale factor that should not be introdcued into the
        // child_propagation_form
        object.renderer_form = object.renderer_form * parent.child_propagation_form;
        object.child_propagation_form =
            object.child_propagation_form * parent.child_propagation_form;

        object
    }

    /// Constructs an Object with identity matrices as transformations.
    pub fn new_empty() -> Object
    {
        let identity = Matrix4::one();
        Object
        {
            renderer_form: identity,
            child_propagation_form: identity,
        }
    }
}

impl RawObject
{
    pub fn translate(&mut self, x: f32, y: f32)
    {
        self.center.x += x;
        self.center.y += y;
    }

    /// Sets position of an object relative to its parent.
    pub fn set_position(&mut self, x: f32, y: f32)
    {
        self.center.x = x;
        self.center.y = y;
    }

    /// Rotates an object relative to its current rotation.
    pub fn rotate(&mut self, rad: f32)
    {
        let rotation = Matrix2::from_angle(Rad(rad));
        self.basis = self.basis * rotation;
    }

    /// Sets a rotation of an object relative to its parent.
    pub fn set_rotation(&mut self, rad: f32)
    {
        self.basis = Matrix2::from_angle(Rad(rad));
    }

    /// Sets x and y halfwidths of object.
    pub fn set_size(&mut self, x: f32, y: f32)
    {
        self.xy_halfwidths.x = x;
        self.xy_halfwidths.y = y;
    }

    /// Adds to x and y halfwidths of object by specified amounts.
    pub fn scale(&mut self, x: f32, y: f32)
    {
        self.xy_halfwidths.x += x;
        self.xy_halfwidths.y += y;
    }
}
