use cgmath::Matrix4;
use cgmath::Vector4;
use cgmath::vec4;
use cgmath::Zero;
use cgmath::SquareMatrix;
use cgmath::Rad;


// ******************** Definitions ********************
pub struct ObjectRaw 
{
    pub width: f32,
    pub height: f32,
    pub x_pos: f32,
    pub y_pos: f32,
    pub z_pos: f32,
    pub z_order: usize,
    pub rotation: f32,
    pub updated: bool,
}

pub struct Object 
{
    pub propagation_mat: Matrix4<f32>,
    pub rel_transform_mat: Matrix4<f32>,
}


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ManipulationArea 
{
    None, Center, Rotation, 
    North, NorthEast, East, SouthEast,
    South, SouthWest, West, NorthWest,
    UI
}

// ******************** Object Raw ********************
impl ObjectRaw {
    pub fn new(x_pos: f32, y_pos: f32, z_pos: f32, z_order: usize,
               width: f32, height: f32) -> ObjectRaw {
        ObjectRaw {
            width: width, height: height,
            x_pos: x_pos, y_pos: y_pos, z_pos: z_pos,
            z_order: z_order,
            rotation: 0.0,
            updated: true,
        }
    } 


    pub fn translation_mat(&self) -> Matrix4<f32> {
        let mut translation_mat: Matrix4<f32> = Matrix4::identity();
        // Translations
        translation_mat.w.x = self.x_pos;
        translation_mat.w.y = self.y_pos;;
        translation_mat
    }


    pub fn rotation_mat(&self) -> Matrix4<f32> {
        Matrix4::from_angle_z(Rad(self.rotation))
    }


    pub fn transform_mat(&self) -> Matrix4<f32> {
        let mut translation_mat = self.translation_mat();
        translation_mat.w.z = self.z_pos;
        let mut scale_mat: Matrix4<f32> = Matrix4::identity();
        scale_mat.x.x = self.width / 2.0;
        scale_mat.y.y = self.height / 2.0;
        // scale_mat.z.z = self.z_pos;

        // self.translation_mat() * self.rotation_mat() * scale_mat
        translation_mat * self.rotation_mat() * scale_mat
    }
}


// ******************** Object (matrices) ********************
impl Object {
    pub fn new(raw: &ObjectRaw, parent_prop_mat: Matrix4<f32>) -> Object {
        Object {
            propagation_mat:
            parent_prop_mat * raw.translation_mat() * raw.rotation_mat(),
            rel_transform_mat: parent_prop_mat * raw.transform_mat(),
        }
    }


    pub fn new_at_root(raw: &ObjectRaw) -> Object {
        Object {
            propagation_mat: raw.translation_mat() * raw.rotation_mat(),
            rel_transform_mat: raw.transform_mat(),
        }
    }


    fn is_inside_square(transform_mat: Matrix4<f32>, x: f32, y: f32) -> bool {
        // Tests by checking if point is to left side of all edges
        // See whether (x,y) lies on on left of edge (x1,y1) - (x2,y2)
        let mut corners: [Vector4<f32>; 4] =
            [Vector4::zero(), Vector4::zero(), Vector4::zero(), Vector4::zero()];
        corners[3] = transform_mat * vec4(1.0, 1.0, 0.0, 1.0);
        corners[2] = transform_mat * vec4(1.0, -1.0, 0.0, 1.0);
        corners[1] = transform_mat * vec4(-1.0, -1.0, 0.0, 1.0);
        corners[0] = transform_mat * vec4(-1.0, 1.0, 0.0, 1.0);
        for i in 0..4 {
            let j = (i + 1) % 4;
            let a = -(corners[j].y - corners[i].y);
            let b = corners[j].x - corners[i].x;
            let c = -(a * corners[i].x + b * corners[i].y);
            let d = -(a * x + b * y + c);
            if d > 0.0 {
                return false
            }
        }
        true
    }


    pub fn mouse_on(&self, x: f32, y: f32) -> bool {
        Object::is_inside_square(self.rel_transform_mat, x, y)
    }

}
