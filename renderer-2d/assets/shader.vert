#version 330

layout(location = 0) in vec2 position;

// smooth out vec4 the_color;

// uniform float z_pos;
uniform mat4 camera_to_clip_matrix;
uniform mat4 world_to_camera_matrix;
uniform mat4 model_to_world_matrix;

void main() {
  vec4 camera_pos =
    world_to_camera_matrix * model_to_world_matrix * vec4(position, 0.0, 1.0);
  gl_Position = camera_to_clip_matrix * camera_pos;
	// the_color = color;
}
