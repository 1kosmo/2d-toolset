#version 330

uniform sampler2D tex;
uniform vec4 colour;

in vec2 v_tex_coords;
// in vec4 v_colour;
out vec4 f_colour;

void main() {
  f_colour = texture(tex, v_tex_coords); // * vec4(1.0, 1.0, 1.0, 1.0);
}
