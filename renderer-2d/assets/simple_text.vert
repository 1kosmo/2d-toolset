#version 330

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 tex_coords;
// layout(location = 2) in vec4 colour;

out vec2 v_tex_coords;
// out vec4 v_colour;

uniform mat4 camera_to_clip_matrix;
uniform mat4 world_to_camera_matrix;
uniform mat4 model_to_world_matrix;

void main() {
  vec4 camera_pos =
    world_to_camera_matrix * model_to_world_matrix * vec4(position, 0.0, 1.0);
  // gl_Position = vec4(position, 0.0, 1.0);
  gl_Position = camera_to_clip_matrix * camera_pos;
  v_tex_coords = tex_coords;
  // v_colour = colour;
}
