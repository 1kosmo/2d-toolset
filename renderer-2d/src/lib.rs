////////// 3rd Party //////////
extern crate gl;
use gl::types::*;

extern crate cgmath;
use cgmath::Matrix;
use cgmath::Matrix4;
use cgmath::SquareMatrix;
use cgmath::Zero;

////////// STD Lib //////////
use std::ffi::CString;
use std::os::raw::c_void;
// use std::sync::Arc;
// use std::sync::RwLock;
// use std::ops::DerefMut;

////////// My Libs //////////
mod initializer;
mod shape;
mod texture_mapper;

use self::shape::Shape;
use texture_mapper::{TextureMapper, TextureID}; 

//////////////////// PUB TYPES ////////////////////
pub type ObjectType = usize;
pub const SQUARE: ObjectType = 0;
// pub const CIRCLE: ObjectType = 0;

pub struct Renderer 
{
    texture_mapper: TextureMapper<(u8, u8, u8, u8)>,
    shader_program_default: GLuint,
    shader_program_textured: GLuint,

    // VAO structure that stores the gluint, size of shape (in indeces)
    // Each Shape struct holds instances of different shapes
    // 0: Squares
    // 1: Circles
    // 2: 
    shapes: Vec<Shape>,

    // Uniforms: 
    // Main shader 
    def_model_to_world_unif: GLint,
    def_world_to_camera_unif: GLint,
    def_camera_to_clip_unif: GLint,
    def_color_unif: GLint,
    def_z_pos_unif: GLint,
    // Textured unif
    tex_model_to_world_unif: GLint,
    tex_world_to_camera_unif: GLint,
    tex_camera_to_clip_unif: GLint,
    tex_data_unif: GLint,

    // Parameters
    win_width: u32,
    win_height: u32,
    z_near: GLfloat,
    z_far: GLfloat,
    camera_pos_x: GLfloat,
    camera_pos_y: GLfloat,
    camera_zoom_scale: GLfloat,
    // frustum_scale: GLfloat,

    world_to_camera_mat: cgmath::Matrix4<f32>,
    camera_to_clip_mat: cgmath::Matrix4<GLfloat>,
}

impl Renderer 
{
    pub fn new(win_width: u32, win_height: u32,
               load_function: &Fn(&str) -> *const c_void)
               -> Renderer
    {
        // Bind OpenGL context to window
        gl::load_with(load_function);

        let mut shapes = Vec::new();
        shapes.push(Shape::new_square());
        
        let mut new = Renderer 
        {
            texture_mapper: TextureMapper::<(u8, u8, u8, u8)>::new(),
            shader_program_default: initializer::initialize_program(
                "src/shaders/shader.vert", "src/shaders/shader.frag"),
            shader_program_textured: initializer::initialize_program(
                "src/shaders/simple_text.vert", "src/shaders/simple_text.frag"),
            shapes: shapes,

            // Main shader 
            def_model_to_world_unif: 0,
            def_world_to_camera_unif: 0, 
            def_camera_to_clip_unif: 0,
            def_color_unif: 0,
            def_z_pos_unif: 0,
            // Textured unif
            tex_model_to_world_unif: 0,
            tex_world_to_camera_unif: 0,
            tex_camera_to_clip_unif: 0,
            tex_data_unif: 0, 

            win_width: win_width,
            win_height: win_height,
            z_near: 1.0,
            z_far: 0.0,
            camera_pos_x: 0.0,
            camera_pos_y: 0.0,
            camera_zoom_scale: 1.0,
            // frustum_scale: Renderer::calc_frustum_scale(45.0),
            world_to_camera_mat: cgmath::Matrix4::identity(),
            camera_to_clip_mat: cgmath::Matrix4::zero(),
        };

        new.texture_mapper.init_after_opengl_context_created();
        new.texture_mapper.prepare_for_use();

        new.get_uniform_locations();
        unsafe 
        {
            gl::Enable(gl::CULL_FACE);
            gl::CullFace(gl::BACK);
            gl::FrontFace(gl::CW);

            gl::Enable(gl::DEPTH_TEST);
            gl::DepthMask(gl::TRUE);
            gl::DepthFunc(gl::LEQUAL);
            gl::DepthRange(0.0, 1.0);

            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
        }
        return new
    }

    fn get_uniform_locations(&mut self) 
    { unsafe {
        // Default
        self.def_model_to_world_unif = gl::GetUniformLocation(
            self.shader_program_default,
            CString::new("model_to_world_matrix").unwrap().as_ptr());
        self.def_world_to_camera_unif = gl::GetUniformLocation(
            self.shader_program_default,
            CString::new("world_to_camera_matrix").unwrap().as_ptr());
        self.def_camera_to_clip_unif = gl::GetUniformLocation(
            self.shader_program_default,
            CString::new("camera_to_clip_matrix").unwrap().as_ptr());
        self.def_color_unif = gl::GetUniformLocation(
            self.shader_program_default,
            CString::new("the_color").unwrap().as_ptr());
        self.def_z_pos_unif = gl::GetUniformLocation(
            self.shader_program_default,
            CString::new("z_pos").unwrap().as_ptr());

        // Textured
        self.tex_model_to_world_unif = gl::GetUniformLocation(
            self.shader_program_textured,
            CString::new("model_to_world_matrix").unwrap().as_ptr());
        self.tex_world_to_camera_unif = gl::GetUniformLocation(
            self.shader_program_textured,
            CString::new("world_to_camera_matrix").unwrap().as_ptr());
        self.tex_camera_to_clip_unif = gl::GetUniformLocation(
            self.shader_program_textured,
            CString::new("camera_to_clip_matrix").unwrap().as_ptr());
        self.tex_data_unif = gl::GetUniformLocation(
            self.shader_program_textured,
            CString::new("tex").unwrap().as_ptr());

        // ORTHOGRAPHIC PROJECTION 
        self.camera_to_clip_mat.x[0] = 2.0 / self.win_width as f32; 
        self.camera_to_clip_mat.y[1] = 2.0 / self.win_height as f32;
        // self.camera_to_clip_mat.x[0] = 1.0; 
        // self.camera_to_clip_mat.y[1] = 1.0;
        // self.camera_to_clip_mat.z[2] = 1.0;
        self.camera_to_clip_mat.z[2] = 2.0 / (self.z_far - self.z_near);
        self.camera_to_clip_mat.w[2] = 
            -(self.z_far + self.z_near) / (self.z_far - self.z_near);
        self.camera_to_clip_mat.w[3] = 1.0;
        
        // Initial camera and window sizings to set uniforms
        self.move_camera(0.0, 0.0, 0.0);
        self.resize_window();

        println!("Camera-to-clip matrix looks like {:?}", self.camera_to_clip_mat);
        gl::UseProgram(self.shader_program_default);
        let color: [GLfloat; 4] = [1.0, 1.0, 1.0, 1.0];
        gl::Uniform4fv(self.def_color_unif, 1, color.as_ptr());
        gl::UseProgram(0);
    } }

    pub fn add_texture(&mut self, texture: Vec<(u8, u8, u8, u8)>,
                       width: i32, height: i32)
                       -> TextureID
    {
        self.texture_mapper.add_texture(texture, width, height)
    }

    pub fn display(&mut self)
    { unsafe {
        gl::ClearColor(0.85, 0.85, 0.85, 0.0);
        // gl::ClearColor(1.0, 1.0, 1.0, 0.0);
        gl::ClearDepth(1.0);
        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

        self.draw();

        let error: GLenum;
        error = gl::GetError();
        match error 
        {
            gl::NO_ERROR => {}
            _ => panic!{"EXITED BECAUSE GetError FOUND AN ERROR"}
        }
    } }

    fn draw(&mut self)
    { unsafe {
        // TODO: Use instanced rendering to render all of these.
        // Start drawing
        for shape in &self.shapes 
        {
            gl::BindVertexArray(shape.vao);

            // Draw untextured objects first
            gl::UseProgram(self.shader_program_default);
            for uniforms in shape.get_untextured_instances().values() 
            {
                gl::UniformMatrix4fv(self.def_model_to_world_unif, 1, gl::FALSE,
                                     uniforms.transform_mat.as_ptr());
                // gl::Uniform1f(self.z_pos_unif, uniforms.z_pos);
                // println!("Drawing object with transform mat that looks like: {:?}; and color that is: {:?}", uniforms.transform_mat, uniforms.color);
                gl::Uniform4fv(self.def_color_unif, 1, uniforms.color.as_ptr());
                gl::DrawArrays(gl::TRIANGLES, 0, 6);
            }

            // Draw textured objects
            gl::UseProgram(self.shader_program_textured);
            // Don't repeat textures
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S,
                              gl::CLAMP_TO_BORDER as GLint);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T,
                              gl::CLAMP_TO_BORDER as GLint);
            // gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S,
            //                   gl::CLAMP_TO_EDGE as GLint);
            // gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T,
            //                   gl::CLAMP_TO_EDGE as GLint);
            // Set linear texture interpolation
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER,
                              gl::NEAREST as GLint);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER,
                              gl::NEAREST as GLint);
            // Mipmaps? Use them to speed things up once you know how to properly
            //          do it for texture atlases
            // Actually, they are only used for 3D, don't use on 2D texture atlases
            // gl::GenerateMipmap(gl::TEXTURE_2D);

            // Enable use of textures-enable the texture vertices buffer for shader
            gl::EnableVertexAttribArray(1); 

            for uniforms in shape.get_textured_instances().values() 
            {
                // Load texture
                self.texture_mapper.use_texture(&uniforms.texture_id,
                                                &shape.vao, &self.tex_data_unif);
                             
                gl::UniformMatrix4fv(self.tex_model_to_world_unif, 1, gl::FALSE,
                                     uniforms.transform_mat.as_ptr());
                // gl::Uniform4fv(self.tex_color_unif, 1, uniforms.color.as_ptr());
                gl::DrawArrays(gl::TRIANGLES, 0, 6);
            }
            gl::DisableVertexAttribArray(1);
        }
        gl::BindVertexArray(0);        
        gl::UseProgram(0);
    } }

    pub fn add_object(&mut self, object_type: usize, object_id: u32,
                      transform_matrix: cgmath::Matrix4<GLfloat>,
                      color: [GLfloat; 4], texture_id: TextureID) 
    {
        self.shapes[object_type]
            .add_instance(object_id, transform_matrix, color, texture_id);
    }

    pub fn delete_object(&mut self, object_type: usize, object_id: u32) {
        self.shapes[object_type].delete_instance(object_id);
    }

    // fn calc_frustum_scale(fov_deg: f32) -> f32 {
    //     let deg_to_rad: f32 = std::f32::consts::PI * 2.0 / 360.0;
    //     let fov_rad = fov_deg * deg_to_rad;
    //     return 1.0 / f32::tan(fov_rad / 2.0)
    // }
    
    pub fn move_camera(&mut self, x_magnitude: f32, y_magnitude: f32,
                       zoom_scale_change: f32)
    {
        self.camera_pos_x += x_magnitude;
        self.camera_pos_y += y_magnitude;
        self.camera_zoom_scale += zoom_scale_change;
        // At first call of this function mat is identity
        self.world_to_camera_mat.x[0] = self.camera_zoom_scale; 
        self.world_to_camera_mat.y[1] = self.camera_zoom_scale;
        self.world_to_camera_mat.w[0] = -self.camera_pos_x;
        self.world_to_camera_mat.w[1] = -self.camera_pos_y;

        println!("World to camera mat looks like: {:?}", self.world_to_camera_mat);

        unsafe {
            // Update self
            gl::UseProgram(self.shader_program_default);
            gl::UniformMatrix4fv(self.def_world_to_camera_unif, 1, gl::FALSE,
                                 self.world_to_camera_mat.as_ptr());
            // Update shaders in text_renderer
            gl::UseProgram(self.shader_program_textured);
            gl::UniformMatrix4fv(self.tex_world_to_camera_unif, 1, gl::FALSE,
                                 self.world_to_camera_mat.as_ptr());
            gl::UseProgram(0);
        }
    }

    fn resize_window(&mut self) {
        // self.camera_to_clip_mat.x[0] = self.frustum_scale /
        //     ((self.win_width as f32) / (self.win_height as f32));
        self.camera_to_clip_mat.x[0] = 2.0 / self.win_width as f32;
        unsafe {
            // Update self
            gl::UseProgram(self.shader_program_default);
            gl::UniformMatrix4fv(self.def_camera_to_clip_unif, 1, gl::FALSE,
                                 self.camera_to_clip_mat.as_ptr());
            // Update shaders in text_renderer
            gl::UseProgram(self.shader_program_textured);
            gl::UniformMatrix4fv(self.tex_camera_to_clip_unif, 1, gl::FALSE,
                                 self.camera_to_clip_mat.as_ptr());
            gl::UseProgram(0);
        }
    }

    pub fn get_win_coord_to_world(&self) -> Matrix4<f32> {
        let mut win_coord_to_world: Matrix4<f32> = Matrix4::identity();
        win_coord_to_world.x[0] = 1.0 / self.camera_zoom_scale;
        win_coord_to_world.y[1] = -1.0 / self.camera_zoom_scale;
        win_coord_to_world.w[0] =
            -(self.win_width as f32 / (2.0 * self.camera_zoom_scale));
        win_coord_to_world.w[1] =
            self.win_height as f32 / (2.0 * self.camera_zoom_scale);

        win_coord_to_world
    }
}




