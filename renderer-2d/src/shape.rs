use std;
use std::mem;
use gl;
use gl::types::*;
use cgmath::Matrix4;
use std::collections::HashMap;

use texture_mapper::TextureID;

//////////////////// PUB TYPES ////////////////////

pub struct Shape {
    pub vao: GLuint,
    pub size: i32,

    instance_registry: HashMap<u32, InstanceType>,
    textured_instances: HashMap<u32, ShapeUniforms>,
    untextured_instances: HashMap<u32, ShapeUniforms>, 
}

impl Shape {
    
    pub fn new_square() -> Shape 
    {
        let mut vao: GLuint = 0;
        let vbo: GLuint;
        let square_vertices: [GLfloat; 12]  = [1.0, 1.0,    // Top right
                                               1.0, -1.0,   // Bottom right
                                               -1.0, 1.0,   // Top Left
                                               -1.0, 1.0,   // Top Left
                                               1.0, -1.0,   // Bottom right
                                               -1.0, -1.0]; // Bottom left
        unsafe 
        {
            vbo = initialize_vertex_buffer(
                mem::transmute(&square_vertices[0]),
                (square_vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr);
            gl::GenVertexArrays(1, &mut vao);
            set_attrib_pointers(&vao, &vbo);
        }
        println!("Initializing vao ({:?}) and vbo ({:?})", vao, vbo);
        Shape 
        {
            vao: vao, size: mem::size_of_val(&square_vertices) as i32,
            instance_registry: HashMap::new(),
            textured_instances: HashMap::new(), 
            untextured_instances: HashMap::new(),
        }
    }
}

// A struct to hold the uniforms together
pub struct ShapeUniforms 
{
    pub transform_mat: Matrix4<GLfloat>,
    // pub z_pos: GLfloat, // z-pos is now being updated through transform mat
    pub color: [GLfloat; 4],
    pub texture_id: TextureID,
}


impl ShapeUniforms 
{
    pub fn new(transform_mat: Matrix4<GLfloat>, color: [GLfloat; 4],
               texture_id: TextureID)
               -> ShapeUniforms
    {
        ShapeUniforms { transform_mat: transform_mat, color: color,
                        texture_id: texture_id }
    }
}

enum InstanceType
{
    UNTEXTURED,
    TEXTURED,
}

//////////////////// IMPL ////////////////////

impl Shape
{
    pub fn add_instance(&mut self, id: u32,
                        transform_matrix: Matrix4<GLfloat>, color: [GLfloat; 4],
                        texture_id: TextureID)
    {
        if texture_id == 0
        {
            self.instance_registry.insert(id, InstanceType::UNTEXTURED);
            self.untextured_instances.insert
                (id, ShapeUniforms::new(transform_matrix, color, texture_id));
        }
        else
        {
            self.instance_registry.insert(id, InstanceType::TEXTURED);
            self.textured_instances.insert
                (id, ShapeUniforms::new(transform_matrix, color, texture_id));
        }
    }

    pub fn delete_instance(&mut self, id: u32) 
    {
        if let Some(instance_type) = self.instance_registry.remove(&id)
        {
            match instance_type
            {
                InstanceType::UNTEXTURED => { self.untextured_instances.remove(&id); },
                InstanceType::TEXTURED => { self.textured_instances.remove(&id); },
            }
        }
        else
        {
            println!("Tried to delete object instance in renderer with id {}", id);
        }
    }

    pub fn get_textured_instances(&self) -> &HashMap<u32, ShapeUniforms>
    {
        return &self.textured_instances
    }

    pub fn get_untextured_instances(&self) -> &HashMap<u32, ShapeUniforms>
    {
        return &self.untextured_instances
    }
}

pub fn initialize_vertex_buffer(vertices: *const std::os::raw::c_void,
                                vert_size: GLsizeiptr) -> GLuint
{
    let mut vbo: GLuint = 0;
    unsafe {
        gl::GenBuffers(1, &mut vbo);

        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(gl::ARRAY_BUFFER, vert_size, vertices, gl::STATIC_DRAW);
        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    }
    vbo
}


// TODO: add new shapes and use index buffers for them
// fn initialize_index_buffer(indeces: *const std::os::raw::c_void,
//                            ind_size: GLsizeiptr) -> GLuint
// {
//     let mut ido: GLuint = 0;
//     unsafe {
//         gl::GenBuffers(1, &mut ido);

//         gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ido);
//         gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, ind_size, indeces, gl::STATIC_DRAW);
//         gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
//     }
//     ido
// }


fn set_attrib_pointers(vao: &GLuint, shape_vbo: &GLuint)
{ unsafe {
    gl::BindVertexArray(*vao);

    gl::BindBuffer(gl::ARRAY_BUFFER, *shape_vbo);
    gl::EnableVertexAttribArray(0);
    gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE, 0, std::ptr::null());

    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    gl::BindVertexArray(0);
} }
