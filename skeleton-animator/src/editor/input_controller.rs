// TODO: figure out why this layer is necessary or even helpful.

use editor::Action;

use editor::sdl2;
use editor::sdl2::event::Event;
use editor::sdl2::keyboard::Keycode;
use editor::sdl2::mouse::MouseButton;


pub struct InputController {
    event_pump: sdl2::EventPump,
}


impl InputController {
    pub fn new(event_pump: sdl2::EventPump) -> InputController {
        InputController {
            event_pump: event_pump,
        }
    }

    pub fn handle_input(&mut self) -> Vec<Action> {
        let (cap_size, _) = self.event_pump.poll_iter().size_hint();
        let mut to_ret = Vec::with_capacity(cap_size);
        for event in self.event_pump.poll_iter() {
            match event {
                Event::KeyDown{ keycode, .. } => {
                    let action = InputController::handle_key_down(keycode);
                    if action.is_some() {
                        to_ret.push(action.unwrap())
                    }
                },
                Event::MouseButtonDown{ mouse_btn, x, y, .. } => {
                    let action = InputController::handle_mouse_button_down(
                        mouse_btn, x, y);
                    if action.is_some() {
                        to_ret.push(action.unwrap())
                    }
                },
                Event::MouseButtonUp{ mouse_btn, x, y, .. } => {
                    let action = InputController::handle_mouse_button_up(
                        mouse_btn, x, y);
                    if action.is_some() {
                        to_ret.push(action.unwrap())
                    }
                },
                Event::MouseMotion{ x, y, xrel, yrel, .. } => {
                    to_ret.push(Action::MouseMotion{
                        x: x, y: y, x_rel: xrel, y_rel: yrel
                    });
                },
                _ => {}
            }
        }

        return to_ret
    }

    fn handle_key_down(key: Option<Keycode>) -> Option<Action> {
        if key.is_none() {
            println!("Got invalid keycode");
            return None
        }
        let key = key.unwrap();
        match key {
            Keycode::Escape => {
                println!("Exiting!");
                Some(Action::Exit)
            },
            _ => {
                println!("{:?}", key);
                None
            }
        }
    }


    fn handle_mouse_button_down(button: MouseButton, x_pos: i32, y_pos: i32)
                                -> Option<Action> {
        match button {
            MouseButton::Left => {
                Some(Action::MLeftDown{ x: x_pos, y: y_pos })
            },
            _ => { None }
        }
    }


    fn handle_mouse_button_up(button: MouseButton, x_pos: i32, y_pos: i32)
                              -> Option<Action> {
        match button {
            MouseButton::Left => {
                Some(Action::MLeftUp{ x: x_pos, y: y_pos })
            },
            _ => { None }
        }
    }
}
