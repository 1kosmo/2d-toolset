////////// 3rd Party //////////
extern crate sdl2;
extern crate cgmath;

use self::sdl2::mouse::Cursor;
use self::sdl2::mouse::SystemCursor;

////////// STD Lib //////////
use std::f32;
use self::cgmath::Matrix4;
use std::sync::Arc;
use std::sync::RwLock;
use std::ops::DerefMut;

////////// My Libs //////////
extern crate renderer_2d;
extern crate object_manipulator;

mod initialize;
mod input_controller;
mod timer;
mod ui;

use self::renderer_2d::Renderer;
use self::object_manipulator::controller::ObjectController;
// use self::object_manipulator::ManipulationArea;
use self::input_controller::InputController;
use self::timer::Timer;


//////////////////// PUB TYPES ////////////////////

pub struct Editor {
    sdl_context: sdl2::Sdl,
    sdl_window: sdl2::video::Window,
    gl_context: sdl2::video::GLContext,
    win_width: u32,
    win_height: u32,
    
    renderer: Renderer,
    timer: Timer,

    object_controller: ObjectController,
    input_controller: InputController,
    // test_obj: u32,

    win_coord_transform: Matrix4<f32>,

    focus_object: u32,
    focus_state: ActionState,
    waiting_state: WaitingState, 
    cursor_type: SystemCursor,
    cursor: Cursor,
}

impl Editor
{
    pub fn new(win_width: u32, win_height: u32) -> Editor {
        let (cont, win, gl_cont) =
            initialize::set_up_context(win_width, win_height);
        let event_pump = cont.event_pump().unwrap();
        
        // Making renderer initialized OpenGL
        let renderer = Renderer::new(
            win_width, win_height,
            &|name| cont.video().unwrap().gl_get_proc_address(name) as *const _
        );
        
        let win_coord_to_world = renderer.get_win_coord_to_world();
        let mut editor = Editor 
        {
            sdl_context: cont,
            sdl_window: win,
            gl_context: gl_cont, // Have to store context in order to keep it alive.
            // Window won't show anything without it.
            win_width: win_width,
            win_height: win_height,
            renderer: renderer,
            timer: Timer::new(62),
            object_controller: ObjectController::new(),
            input_controller: InputController::new(event_pump),
            win_coord_transform: win_coord_to_world,
            focus_object: 0,
            // ProgressDrawing{
            // id: 0, pivot_point_x: 0.0, pivot_point_y: 0.0},
            focus_state: ActionState::None,
            waiting_state: WaitingState::None,
            cursor_type: SystemCursor::Arrow,
            cursor: Cursor::from_system(SystemCursor::Arrow).unwrap(),
        };
        editor.initialize_ui();

        editor
    }
}

pub enum Action 
{
    Exit,
    MLeftDown {
        x: i32,
        y: i32,
    },
    MLeftUp {
        x: i32,
        y: i32,
    },
    MouseMotion {
        x: i32,
        y: i32,
        x_rel: i32,
        y_rel: i32,
    },
}

#[derive(PartialEq)]
pub enum ActionState {
    None,
    Draw,
    // Move,
    Resize,
    Rotate
}


#[derive(PartialEq)]
pub enum WaitingState {
    None,
    Link,
}

//////////////////// IMPL ////////////////////

impl Editor 
{
    pub fn execute(&mut self) 
    {
        'main: loop 
        {
            // println!("Before using texture_mapper");
            self.texture_mapper.write().unwrap().prepare_for_use();
            // println!("After preparing texture_mapper");

            self.renderer.display();
            self.sdl_window.gl_swap_window();

            // Use input
            // (Try using it before updating renderer)
            if self.handle_actions() 
            {
                break 'main
            }

            // Update renderer
            for (id, mat, color, tex_id) in
                self.object_controller.get_object_params()
            {
                self.renderer.add_object(renderer::SQUARE, id, mat, color, tex_id);
            }

            // Update FPS counter/throttler
            self.timer.tick();
        }
    }


    // Returns true if exit status reached 
    fn handle_actions(&mut self) -> bool {
        for action in self.input_controller.handle_input() {
            match action {
                Action::Exit => { return true },
                Action::MLeftDown{x, y} => {
                    self.handle_click(x, y);
                },
                Action::MLeftUp{x, y} => {
                    self.handle_click_release();
                },
                Action::MouseMotion{x, y, x_rel, y_rel} => {
                    self.handle_mouse_motion(x, y, x_rel, y_rel);
                },
            }
        }

        return false
    }


    // ******************** EXECUTION HELPER FUNCTIONS ********************
    fn screen_to_world(&self, x: i32, y: i32) -> (f32, f32) 
    {
        let coords = cgmath::Vector4::new(x as f32, y as f32, 0.0, 1.0);
        let transformed_coords = self.win_coord_transform * coords;
        (transformed_coords.x, transformed_coords.y)
    }


    fn handle_click(&mut self, x: i32, y: i32) 
    {
        let (x, y) = self.screen_to_world(x, y);
        // Check object controllers to see if any triggers clicked
        let (clicked_on_id, clicked_on_area) =
            self.object_controller.mouse_is_clicked_on(x, y);
        if self.waiting_state == WaitingState::None &&
            self.focus_state == ActionState::None
        {
            if clicked_on_id == 0 {
                self.clicked_nothing(x, y);
            } else if clicked_on_area == ManipulationArea::UI {
                self.clicked_ui(clicked_on_id);
            } else {
                self.clicked_object(clicked_on_id);
            }
        } 
        else if self.waiting_state == WaitingState::Link 
        {
            if clicked_on_area == ManipulationArea::Center &&
                self.focus_object != 0
            {
                self.object_controller
                    .link_objects(&clicked_on_id, self.focus_object);
            }
            // Always reset waiting state to nothing after one click
            self.waiting_state = WaitingState::None;
        }

        if self.focus_state != ActionState::None &&
            self.focus_state != ActionState::Rotate 
        {
                self.sdl_context.mouse().show_cursor(false);
        }
    }


    fn clicked_nothing(&mut self, x: f32, y: f32) 
    {
        // Makes it necessary to deselect an object before creating new
        if self.focus_object != 0 
        {
            self.focus_state = ActionState::None;
            self.focus_object = 0;
            self.deselect_object();
        }
        else 
        {
            self.object_controller.draw_object(0, x, y);
            self.focus_state = ActionState::Draw;
        }
    }


    fn clicked_object(&mut self, clicked_on_id: u32) 
    {
        if self.focus_object != clicked_on_id 
        {
            self.focus_object = clicked_on_id;
            self.select_object();
        }
    }


    fn handle_click_release(&mut self) 
    {
        if self.focus_state == ActionState::Draw 
        {
            self.object_controller.clear_selected();
        }
        self.focus_state = ActionState::None;
        self.object_controller.clear_selected_manip_area();
        self.sdl_context.mouse().show_cursor(true);
    }


    fn handle_mouse_motion(&mut self, x: i32, y: i32, x_rel: i32, y_rel: i32) 
    {
        let (x, y) = self.screen_to_world(x, y);
        let x_rel = x_rel as f32;
        let y_rel = -y_rel as f32;
        self.object_controller.adjust_object(x, y, x_rel, y_rel);

        match self.focus_state 
        {
            ActionState::Resize => {
                self.object_controller.update_selected_obj_addons();
            },
            _ => {}
        }
    }


    fn delete_object(&mut self, id: u32) 
    {
        self.object_controller.delete_object(&id);
        self.renderer.delete_object(0, id);
    }
}
