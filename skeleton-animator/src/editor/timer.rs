extern crate time;
use self::time::{Timespec, Duration};

use std;

pub struct Timer {
    start_time: Timespec,

    last_taken: Timespec,
    last_interval: Duration,

    fps_current: u8,
    fps_target: u8,
    time_to_sleep: Duration,
    ideal_sleep: Duration,
}


impl Timer {
    pub fn new(given_fps: u8) -> Timer {
        Timer {
            fps_target: given_fps,
            fps_current: 0,
            start_time: time::get_time(),
            last_taken: time::get_time(),
            last_interval: Duration::zero(),
            time_to_sleep: Duration::milliseconds(
                1_000 / ((given_fps as i64) - 1)),
            ideal_sleep: Duration::milliseconds(
                1_000 / (given_fps as i64)),
        }
    }

    pub fn tick(&mut self) {
        let cur: Timespec = time::get_time();
        self.last_interval = self.last_taken - cur;

        if self.last_taken.sec < cur.sec {
            // println!("Got {} frames", self.fps_current);
            self.fps_current = 1;
        } else {
            self.fps_current = self.fps_current + 1;
        }

        if self.ideal_sleep.num_milliseconds() * (self.fps_current as i64) <
            (( cur.nsec as i64) / 1_000_000)
        {
            self.time_to_sleep = self.time_to_sleep / 2;
            std::thread::sleep(self.time_to_sleep.to_std().unwrap());
        } else {
            std::thread::sleep(self.ideal_sleep.to_std().unwrap());
        }

        self.last_taken = cur;
    }


    pub fn interval(&self) -> Duration {
        self.last_interval
    }


    pub fn get_total_time(&self) -> Duration {
        return self.start_time - self.last_taken;
    }
}
